## Project Planner

Project Task Management System

An online project task management system can help individuals achieve goals, or groups collaborate and share knowledge to accomplish collective goals.

## Design

- [Wireframe](https://gitlab.com/yukilmy520/project-planner/-/blob/main/docs/wireframe.png)

## Intended Market

Project Task Management System is a system used to keep projects on track and stakeholders informed throughout the project lifecycle. It provides clarity among teams, keeps your projects within scope, and helps you meet customer needs on time.

## Functionality

- As a user I can sign up or log in and log out.
- As a user I can log in and create a new projects,and name the project's owner.
- As a user I can log in and create a new tasks related to one project, and name the assignee.
- As a user I can log in and leave a note under one specific task.

## Project Initialization

To fully enjoy this application on your local machine, please make sure to follow these steps:

1. Clone the repository down to your local machine
2. CD into the new project directory
3. Create a Python virtual environment, run `python -m venv ./.venv`, activate the virtual environment `./.venv/bin/activate`
4. Upgrade pip and install django, run `python -m pip install --upgrade pip`, `python -m pip install --upgrade pip`
5. Install requirements and capture(freeze) project’s dependencies, run `pip freeze > requirements.txt`
6. Run migrations `python manage.py migrate`
7. Run server `python manage.py runserver`
8. Please visit "localhost:8000/" to explore this application
