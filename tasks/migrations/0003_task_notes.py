# Generated by Django 4.1.2 on 2022-11-02 23:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("tasks", "0002_alter_task_due_date_alter_task_start_date"),
    ]

    operations = [
        migrations.AddField(
            model_name="task",
            name="notes",
            field=models.TextField(null=True),
        ),
    ]
