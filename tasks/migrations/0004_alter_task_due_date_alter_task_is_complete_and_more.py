# Generated by Django 4.1.2 on 2022-11-02 23:40

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ("projects", "0001_initial"),
        ("tasks", "0003_task_notes"),
    ]

    operations = [
        migrations.AlterField(
            model_name="task",
            name="due_date",
            field=models.DateTimeField(
                default=django.utils.timezone.now, null=True
            ),
        ),
        migrations.AlterField(
            model_name="task",
            name="is_complete",
            field=models.BooleanField(default=False, null=True),
        ),
        migrations.AlterField(
            model_name="task",
            name="name",
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name="task",
            name="project",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="tasks",
                to="projects.project",
            ),
        ),
        migrations.AlterField(
            model_name="task",
            name="start_date",
            field=models.DateTimeField(
                default=django.utils.timezone.now, null=True
            ),
        ),
    ]
