from django.db import models
from projects.models import Project
from django.contrib.auth.models import User
from django.utils import timezone


class Task(models.Model):
    name = models.CharField(max_length=200, null=True)
    start_date = models.DateTimeField(default=timezone.now, null=True)
    due_date = models.DateTimeField(default=timezone.now, null=True)
    is_complete = models.BooleanField(default=False, null=True)
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True
    )
    assignee = models.ForeignKey(
        User, related_name="tasks", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.name


class Notes(models.Model):
    notes = models.TextField(null=True)
    task = models.ForeignKey(
        Task,
        related_name="notes",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.notes
