from django.urls import path
from tasks.views import create_task, show_my_tasks, create_note


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("create/note/", create_note, name="create_note"),
]
