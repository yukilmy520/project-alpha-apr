from django import forms
from tasks.models import Task, Notes


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ["name", "start_date", "due_date", "project", "assignee"]


class NoteForm(forms.ModelForm):
    class Meta:
        model = Notes
        fields = ["notes", "task"]
